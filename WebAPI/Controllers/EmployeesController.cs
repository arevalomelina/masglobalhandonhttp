﻿using BusinessModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmployeesController : ApiController
    {
        [HttpGet]
        public IEnumerable<EmployeeDto> Get()
        {
            var response = new ModeloFacade().GetEmployees().Select(p =>
              new EmployeeDto()
              {

                  Id = p.Id,
                  Name = p.Name,
                  ContractTypeName = p.ContractTypeName,
                  RoleId = p.RoleId,
                  RoleName = p.RoleName,
                  RoleDescription = p.RoleDescription,
                  HourlySalary = p.HourlySalary,
                  MonthlySalary = p.MonthlySalary,
                  AnnualSalary = p.AnnualSalary
              }
                );
            return response;
        }

        [HttpGet]
        public EmployeeDto Get(int id)
        {
            var employee = new ModeloFacade().GetEmployeesById(id);
            EmployeeDto response = new EmployeeDto()
            {
                Id = employee.Id,
                Name = employee.Name,
                ContractTypeName = employee.ContractTypeName,
                RoleId = employee.RoleId,
                RoleName = employee.RoleName,
                RoleDescription = employee.RoleDescription,
                HourlySalary = employee.HourlySalary,
                MonthlySalary = employee.MonthlySalary,
                AnnualSalary = employee.AnnualSalary
            };
            return response;
        }
    }
}
