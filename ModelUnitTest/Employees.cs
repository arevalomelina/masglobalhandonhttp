﻿using System;
using System.Collections.Generic;
using BusinessModel;
using BusinessModel.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ModelUnitTest
{
    [TestClass]
    public class Employees
    {
        private ModeloFacade target = new ModeloFacade();

        [TestMethod]
        public void TestGetEmployees()
        {
            List<Employee> result = target.GetEmployees();            
            Assert.IsTrue(result.Count>0);
        }
        [TestMethod]
        public void TestGetEmployeesByIdExists()
        {
            int id = 1;
            Employee result = target.GetEmployeesById(id);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestGetEmployeesByIdNoExists()
        {
            int id = 3;
            Employee result = target.GetEmployeesById(id);
            Assert.IsNull(result);
        }
    }
}
