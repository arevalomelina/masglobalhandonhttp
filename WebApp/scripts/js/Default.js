﻿var employeesAPI = "http://localhost:64563/api/Employees";

$(document).ready(function () {
    $("#EmployeeTable").css('display', 'none');
    $("#EmployeeForm").css('display', 'none');

    $('#btnSearch').on('click', function (ev) {
        var id = $("#txtId").val();
        if (id == '') {
            getEmployees();
        }
        else {
            getEmployeesById(id);
        }
        ev.preventDefault();
        return;
    });
});

function getEmployees() {
    $.ajax({
        type: "GET", // la variable type guarda el tipo de la peticion GET,POST,..
        crossDomain: true,        
        url: employeesAPI, //url guarda la ruta hacia donde se hace la peticion
        success: function (datos) { //success es una funcion que se utiliza si el servidor retorna informacion
            console.log(datos)
            $("#EmployeeTable").css('display', 'block');
            $("#EmployeeForm").css('display', 'none');
            var html = "<h4>Employee List</h4><br><table class='table table-striped table-dark'";
            html += "<thead>";
            html += "<tr><th scope='col'>Id</th>";
            html += "<th scope='col'>Name</th>";
            html += "<th scope='col'>Contract</th>";
            html += "<th scope='col'>Role</th>";
            html += "<th scope='col'>Hourly Salary</th>";
            html += "<th scope='col'>Monthly Salary</th>";
            html += "<th scope='col'>Annual Salary</th>";
            html += "</tr>";
            html += "</thead>";
            html += "<tbody>";
            for (var i = 0; i < datos.length; i++) {
                html += "<tr>";
                html += "<td class='br'>" + datos[i].Id + "</td>";
                html += "<td class='br'>" + datos[i].Name + "</td>";
                html += "<td class='br'>" + datos[i].ContractTypeName + "</td>";
                html += "<td class='br'>" + datos[i].RoleName + "</td>";
                html += "<td class='br'>" + datos[i].HourlySalary + "</td>";
                html += "<td class='br'>" + datos[i].MonthlySalary + "</td>";
                html += "<td class='br'>" + datos[i].AnnualSalary + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            $("#EmployeeTable").append(html);
            
        },
        error: function (e) {
            console.log(e);          
        },
        dataType: "json",
        contentType: "application/json"
    });
}

function getEmployeesById( id) {
    $.ajax({
        type: "GET", // la variable type guarda el tipo de la peticion GET,POST,..
        url: employeesAPI+"/"+id, //url guarda la ruta hacia donde se hace la peticion
        success: function (datos) { //success es una funcion que se utiliza si el servidor retorna informacion
            $("#EmployeeForm").css('display', 'block');
            $("#EmployeeTable").css('display', 'none');
            console.log(datos)
            $("#txtIdForm").val(datos.Id);
            $("#txtName").val(datos.Name);
            $("#txtContract").val(datos.ContractTypeName);
            $("#txtRole").val(datos.RoleName);
            $("#txtHourlySalary").val(datos.HourlySalary);
            $("#txtMonthlySalary").val(datos.MonthlySalary);
            $("#txtAnnualSalary").val(datos.AnnualSalary);
        },
        error: function (e) {
            console.log(e);
        },
        dataType: "json",
        contentType: "application/json"
    });
}