﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.DataAccessLayer
{
    /// <summary>
    /// Allows access to specified API as datasource
    /// </summary>
    internal class DataAccessLogic
    {
        //Local variables to store URI values
        private string URL = ConfigurationManager.AppSettings["MasGlobalAPI"].ToString();
        private string urlParameters = "";

        /// <summary>
        /// Get API to retrieve employees information
        /// </summary>
        /// <returns></returns>
        internal List<EmployeeResult> getEmployees()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            List<EmployeeResult> employees = new List<EmployeeResult>();
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result; 
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<EmployeeResult>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                employees = dataObjects.ToList();
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            client.Dispose();
            return employees;
        }
    }
}
