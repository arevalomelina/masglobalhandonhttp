﻿namespace BusinessModel.DataAccessLayer
{
    /// <summary>
    /// Internal class to map API response.
    /// </summary>
    internal class EmployeeResult
    {
      
            public int id { get; set; }
            public string name { get; set; }
            public string contractTypeName { get; set; }
            public int roleId { get; set; }
            public string roleName { get; set; }
            public string roleDescription { get; set; }
            public decimal hourlySalary { get; set; }
            public decimal monthlySalary { get; set; }
           
    }
}
