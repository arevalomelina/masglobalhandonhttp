﻿using BusinessModel.DataAccessLayer;
using BusinessModel.DTO;
using BusinessModel.ServiceAgent;
using System.Collections.Generic;
using System.Linq;

namespace BusinessModel.BusinessLayer
{
    /// <summary>
    /// Business logic layer that call data access logic to retrieve information about company employees
    /// and consumes a factory to calculate Annual salary
    /// </summary>
    internal class EmployeesBL
    {
        /// <summary>
        /// private variable that makes reference to data logic layer
        /// </summary>
        private DataAccessLogic employeesDAL = new DataAccessLogic();

        /// <summary>
        /// Get the list of all employees on MasGlobal API
        /// </summary>
        /// <returns>List of employees</returns>
        internal List<Employee> GetEmployees()
        {
            //Instace of contract factory to retrieve annual salary according to contrat type
            ContractFactorySA factorySA = new ContractFactorySA();

            //Gets employees from data access and assigne to data transport object using linq
            List<Employee> employees = (from p in employeesDAL.getEmployees()
                                       select new Employee()
                                       {
                                            Id = p.id,
                                            Name = p.name,
                                            ContractTypeName = p.contractTypeName,
                                            RoleId= p.roleId,
                                            RoleName = p.roleName,
                                            RoleDescription = p.roleDescription,
                                            HourlySalary = p.hourlySalary,
                                            MonthlySalary = p.monthlySalary,
                                            AnnualSalary= factorySA.AnnualSalary(p.contractTypeName,p.monthlySalary,p.hourlySalary)
                                       }).ToList();
            return employees;
        }

        /// <summary>
        /// Gets employee information according to provided id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Employee info</returns>
        internal Employee GetEmployeesById( int id)
        {
            ContractFactorySA factorySA = new ContractFactorySA();
            var employees = (from p in employeesDAL.getEmployees()
                                        where p.id == id
                                        select new Employee()
                                        {
                                            Id = p.id,
                                            Name = p.name,
                                            ContractTypeName = p.contractTypeName,
                                            RoleId = p.roleId,
                                            RoleName = p.roleName,
                                            RoleDescription = p.roleDescription,
                                            HourlySalary = p.hourlySalary,
                                            MonthlySalary = p.monthlySalary,
                                            AnnualSalary = factorySA.AnnualSalary(p.contractTypeName, p.monthlySalary, p.hourlySalary)
                                        }).FirstOrDefault();
          
            return employees;
        }
    }
}
