﻿using Factory;
using Factory.DTO;

namespace BusinessModel.ServiceAgent
{
    /// <summary>
    /// Agent class to consume external providers as Contract Factory
    /// </summary>
    internal class ContractFactorySA
    {
        /// <summary>
        /// Retrieves annual salary calculated according to contract typpe
        /// </summary>
        /// <param name="contract">Contract type specified</param>
        /// <param name="monthly">salary per month value</param>
        /// <param name="hourly">salary per hour value</param>
        /// <returns></returns>
        internal decimal AnnualSalary(string contract, decimal monthly, decimal hourly)
        {
            //Contract object request definition.
            FactoryRequest request = new FactoryRequest();
            request.ContractTypeName = contract;
            request.HourlySalary = hourly;
            request.MonthlySalary = monthly;

            //Factory call to calculate annual salary.
            decimal annual = new FactoryFacade().GetAnnualSalary(request);
            return annual;
        }
    }
}
