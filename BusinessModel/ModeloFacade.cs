﻿using BusinessModel.BusinessLayer;
using BusinessModel.DTO;
using System.Collections.Generic;

namespace BusinessModel
{
    /// <summary>
    /// Public method that allows get business logic result
    /// </summary>
    public class ModeloFacade
    {
        /// <summary>
        /// Returns employees list
        /// </summary>
        /// <returns>List of employee</returns>
        public List<Employee> GetEmployees()
        {
            return new EmployeesBL().GetEmployees();
        }

        /// <summary>
        /// Returns employee info by Id
        /// </summary>
        /// <param name="id">Id for requested employee</param>
        /// <returns>Employee info</returns>
        public Employee GetEmployeesById(int id)
        {
            return new EmployeesBL().GetEmployeesById(id);
        }
    }
}
