﻿namespace Factory.DTO
{
    /// <summary>
    /// Data transport object for factory requests
    /// </summary>
    public class FactoryRequest
    {
        public string ContractTypeName { get; set; }
        public decimal HourlySalary { get; set; }
        public decimal MonthlySalary { get; set; }
    }
}
