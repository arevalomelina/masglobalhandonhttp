﻿using Factory.DTO;
using Factory.Interface;

namespace Factory
{
    /// <summary>
    /// Public class to access to factory
    /// </summary>
    public class FactoryFacade : IContractFactory
    {
        /// <summary>
        /// Returns calculated annual salary according to contract type
        /// </summary>
        /// <param name="factoryRequest">Contract object with required parameters</param>
        /// <returns>calculated annual salary</returns>
        public decimal GetAnnualSalary(FactoryRequest factoryRequest)
        {
            IContractFactory contractFactory = ContractFactory.GetContractProvier(factoryRequest.ContractTypeName);
            return contractFactory.GetAnnualSalary(factoryRequest);
        }
    }
}
