﻿using Factory.Adapter;
using Factory.Interface;
using System;

namespace Factory
{
    /// <summary>
    /// static class to retrive specified adapter according to contract type
    /// </summary>
    public static class ContractFactory
    {
        /// <summary>
        /// Static method to retrieve adapter
        /// </summary>
        /// <param name="contractType">contract type name</param>
        /// <returns>Adapter according to contract type</returns>
        public static IContractFactory GetContractProvier(string contractType)
        {
            try {
                IContractFactory contractFactory = null;
                ContractType eProvider = (ContractType)(Enum.Parse(typeof(ContractType), contractType));

                switch (eProvider)
                {
                    case ContractType.HourlySalaryEmployee:
                        contractFactory = new HourlySalaryAdapter();
                        break;

                    case ContractType.MonthlySalaryEmployee:
                        contractFactory = new MonthlySalaryAdapter();
                        break;
                }
                return contractFactory;
                }
            catch {
                return null;
            }

        }


    }

    public enum ContractType
    {
        HourlySalaryEmployee,
        MonthlySalaryEmployee
    }
}
