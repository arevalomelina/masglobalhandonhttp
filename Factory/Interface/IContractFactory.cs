﻿using Factory.DTO;

namespace Factory.Interface
{
    /// <summary>
    /// Public interface for contract types factory
    /// </summary>
    public interface IContractFactory
    {
        /// <summary>
        /// Allows calculate annual salary according to contract type
        /// </summary>
        /// <param name="factoryRequest">Contract object with required parameters</param>
        /// <returns>calculated annual salary</returns>
        decimal GetAnnualSalary(FactoryRequest factoryRequest);
    }
}
