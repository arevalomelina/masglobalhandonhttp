﻿using Factory.DTO;
using Factory.Interface;

namespace Factory.Adapter
{
    /// <summary>
    /// Public adapter for HourlySalary contract
    /// </summary>
    public class HourlySalaryAdapter : IContractFactory
    {
        /// <summary>
        /// Returns annual salary by Hourly contract
        /// </summary>
        /// <param name="factoryRequest">Contract object with required parameters</param>
        /// <returns>calculated annual salary</returns>
        public decimal GetAnnualSalary(FactoryRequest factoryRequest)
        {
            decimal hourly = factoryRequest.HourlySalary;
            decimal annualSalary = 120 * hourly * 12;
            return annualSalary;
        }
    }
}
