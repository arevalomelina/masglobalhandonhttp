﻿using Factory.DTO;
using Factory.Interface;

namespace Factory.Adapter
{
    /// <summary>
    /// Public adapter for MonthlySalary contract
    /// </summary>
    public class MonthlySalaryAdapter : IContractFactory
    {
        /// <summary>
        /// Returns annual salary by monthly contract
        /// </summary>
        /// <param name="factoryRequest">Contract object with required parameters</param>
        /// <returns>Calculated annual salary</returns>
        public decimal GetAnnualSalary(FactoryRequest factoryRequest)
        {
            decimal monthly = factoryRequest.HourlySalary;
            decimal annualSalary = monthly * 12;
            return annualSalary;
        }
    }
}
